﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float time;

    [SerializeField]
    float maxLiveTime = 10;

    [SerializeField]
    GameObject bulletFragmentPrefab;

    void Start()
    {
        time = 0;
    }

    void Update()
    {
        time += Time.deltaTime;

        if(time >= maxLiveTime)
        {
            Destroy(gameObject);
        }
    }

    public void Explode()
    {
        int numberOfFragments = 8;
        float x;
        float y;

        if (bulletFragmentPrefab)
        {
            for (int i = 0; i < numberOfFragments; i++)
            {
                GameObject newBullet = GameObject.Instantiate(bulletFragmentPrefab, this.transform);

                newBullet.transform.parent = null;

                x = Random.Range(0, 360);
                y = Random.Range(0, 360);
                newBullet.transform.rotation = Quaternion.Euler(x, y, 0);

                newBullet.GetComponent<Rigidbody>().AddForce(newBullet.transform.forward * 300);
            }

            Destroy(gameObject);
        }
    }
}
