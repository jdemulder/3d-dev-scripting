﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Zombie : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI PointsText;

    GameManager gm;

    Transform player;

    float speed = 2f;
    Vector3 newPosition;
    
    void Start()
    {
        //ASSIGN POINTS(TEXT)
        
        //ASSIGN GAMEMANAGER
        
        //ASSIGNPLAYER
        
    }

    void Update()
    {
        if (player)
        {
            newPosition = new Vector3(player.position.x, 0, player.position.z);
            this.transform.LookAt(newPosition);
            this.transform.position = Vector3.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            if (gm)
            {
                Debug.Log("Hit!");

                GameObject.Destroy(other.gameObject);
                GameObject.Destroy(gameObject);

                gm.points++;

                PointsText.text = "Points: " + gm.points;
            }
        }
    }
}
